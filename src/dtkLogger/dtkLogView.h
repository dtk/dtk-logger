// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtWidgets>

#include <dtkLoggerExport.h>

class dtkLogViewPrivate;

class DTKLOGGER_EXPORT dtkLogView : public QWidget
{
    Q_OBJECT

public:
     dtkLogView(QWidget *parent = 0);
    ~dtkLogView(void);

public slots:
    void displayTrace(bool display);
    void displayDebug(bool display);
    void displayInfo(bool display);
    void displayWarn(bool display);
    void displayError(bool display);
    void displayFatal(bool display);

protected slots:
    void autoScrollChecked(int state);
    void disableAutoScroll(void);
    void enableAutoScroll(void);

private:
    dtkLogViewPrivate *d;
};

//
// dtkLogView.h ends here
