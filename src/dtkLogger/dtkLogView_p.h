// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtWidgets>

#include "dtkLogger.h"

class dtkLogModel;

// /////////////////////////////////////////////////////////////////
// dtkLogViewBar
// /////////////////////////////////////////////////////////////////

class dtkLogViewBar : public QFrame
{
    Q_OBJECT

public:
    dtkLogViewBar(QWidget *parent = 0);
    ~dtkLogViewBar(void);

signals:
    void displayTrace(bool);
    void displayDebug(bool);
    void displayInfo(bool);
    void displayWarn(bool);
    void displayError(bool);
    void displayFatal(bool);
};

// /////////////////////////////////////////////////////////////////
// dtkLogViewTree
// /////////////////////////////////////////////////////////////////

class dtkLogViewTree : public QTreeWidget
{
    Q_OBJECT

public:
    dtkLogViewTree(QWidget *parent = 0);
    ~dtkLogViewTree(void);

signals:
    void runtimeClicked(void);
    void fileClicked(const QString& path);

protected slots:
    void onItemClicked(QTreeWidgetItem *, int);

private:
    QTreeWidgetItem *runtime;
    QTreeWidgetItem *file;
};

// /////////////////////////////////////////////////////////////////
// dtkLogViewList
// /////////////////////////////////////////////////////////////////

class dtkLogViewList : public QListView
{
    Q_OBJECT

public:
     dtkLogViewList(QWidget *parent = 0);
    ~dtkLogViewList(void);

public slots:
    void setRuntime(void);
    void setFile(const QString& path);
    void setAutoScroll(bool autoScroll);

public:
    void setFilter(const QRegExp& expression);

private:
    dtkLogModel *model;

private:
    QHash<QString, QStringListModel *> models;

private:
    QSortFilterProxyModel *proxy;
};

// /////////////////////////////////////////////////////////////////
// dtkLogViewPrivate
// /////////////////////////////////////////////////////////////////

class dtkLogViewPrivate
{
public:
    QRegExp expression(void);

public:
    dtkLogViewBar  *bar;
    dtkLogViewTree *tree;
    dtkLogViewList *list;

public:
    QStringList exclude;

public:
    QCheckBox *checkbox_auto_scroll;
};

//
// dtkLogView_p.h ends here
